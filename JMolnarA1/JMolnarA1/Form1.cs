﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMolnarA1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDisplayClick(object sender, EventArgs e)
        {
            //string text = txtText.Text;
            //int numbers = int.Parse(txtNumbers.Text);
            //txtAll.Text = text + numbers;
            string err = "";
            try
            {
                txtText.Text = txtText.Text.Trim();
                txtNumbers.Text = txtNumbers.Text.Trim();
                if (txtText.Text == "")
                {
                    err += "Please enter text\n";
                }
                if (txtNumbers.Text == "" || int.TryParse(txtNumbers.Text, out int i) == false)
                {
                    err += "Please enter numbers\n";
                }

                if (err != "") throw new Exception();
                else txtAll.Text = txtText.Text + " " + txtNumbers.Text;
            }catch(Exception)
            {
                MessageBox.Show(err);
                txtText.Clear();
                txtNumbers.Clear();
                txtAll.Clear();
            }
        }
    }
}
